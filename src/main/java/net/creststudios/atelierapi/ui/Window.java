package net.creststudios.atelierapi.ui;

/**
 * NO JAVADOC YET WRITTEN
 */
public interface Window {

    /**
     * @return The current width of the window
     */
    int getWidth();

    /**
     * @param width The width of the window
     */
    void setWidth(int width);

    /**
     * @return The current height of the window
     */
    int getHeight();

    /**
     * @param height The height of the window
     */
    void setHeight(int height);

    /**
     * @return The title of the window
     */
    String getTitle();

    /**
     * @param title The title of the window
     */
    void setTitle(String title);

    /**
     * @return Whether the window is visible to the Operating System
     */
    boolean isVisible();

    /**
     * @param visible Whether the window is visible to the Operating System
     */
    void setVisible(boolean visible);

}
